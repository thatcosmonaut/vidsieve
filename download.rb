require 'youtube_search'
require 'viddl-rb'

def video_download_urls(search_term)
  search = YoutubeSearch.search(search_term).first(5)
  [].tap do |url_list|
    search.each do |result|
      id = result["video_id"]
      url_list << "http://youtube.com/watch?v=#{id}"
    end
  end
end

def download_videos(urls)
  urls.each do |url|
    `viddl-rb #{url} --save-dir #{Dir.pwd}/videos`
  end
end
