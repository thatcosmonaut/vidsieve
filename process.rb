# process:
# dump frames
# recut frames
# shuffle the recut frames
# process frames
# assemble frames

require 'rmagick'
require 'ruby-progressbar'
require 'fileutils'

# videos = Dir.entries('videos').select {|f| !File.directory? f}
#
# `ffmpeg -y -i videos/#{videos.first.dump} -t 60 frames/Frame%06d.png` #end after 60 sec
#
# pbar = ProgressBar.create(title: 'Effects', total: Dir.glob('frames/Frame*.png').count)
# Dir.glob('frames/Frame*.png').each do |frame|
#   frame_img = Magick::Image.read(frame)
#   new_frame = frame_img[0].negate.oil_paint
#   new_frame.write("Converted" + frame)
#   pbar.increment
# end
# pbar.finish
#
# framerate = `ffmpeg -i CharcoalMadhav.mp4 2>&1 | sed -n "s/.*, \(.*\) fp.*/\1/p"`
# `ffmpeg -y -r #{framerate} -i Convertedframes/Frame%06d.png converted_videos/output.mp4` #TODO keep track of framerate
#

def friendly_filename(filename)
    filename.gsub(/[^\w\s_-]+/, '')
            .gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2')
            .gsub(/\s+/, '_')
end

def video_list
  Dir.entries('videos').select {|f| !File.directory? f}
end

def dump_frames(truncate_length)
  `mkdir frames`
  video_list.each_with_index do |video_name, i|
    `ffmpeg -y -i videos/#{video_name.dump} -t #{truncate_length} -s 480x360 -r 25 frames/video#{i}Frame%06d.png`
  end
end

def recut_frames
  cut_indices = cut_list(Dir.glob("frames/*"), 60, 360)
  cut_by_index_list(Dir.glob("frames/*"), cut_indices)
end

def cut_by_index_list(list, index_list)
  list_of_lists = []
  i = 0
  index_list.each do |index|
    sub_list = []
    while i < index
      sub_list << list[i]
      i += 1
    end
    list_of_lists << sub_list
  end

  sub_list = []
  while i < list.length
    sub_list << list[i]
    i += 1
  end
  list_of_lists << sub_list

  list_of_lists
end

def shuffle_cuts(list_of_cuts)
  list_of_cuts.shuffle
end

# takes in a list and returns indices for creating a list of lists
# min_cut 90, max_cut 360 good starting point
def cut_list(list, min_cut_frames, max_cut_frames)
  avg = (min_cut_frames + max_cut_frames) / 2
  cut_indices = (0..list.length-1).step(avg).to_a
  cut_indices.shift
  cut_indices.map do |cut_index|
    cut_index += rand(-(avg/3)+1..(avg/3)-1)
  end
end

def padded_integer(integer, length)
  str = integer.to_s
  ("0" * (length - str.length)) + str
end

def save_frames(frame_list)
  `mkdir assembled_frames`
  digits = frame_length_digit_count(frame_list)
  frame_list.flatten.each_with_index do |frame, index|
    `cp #{frame} assembled_frames/frame#{padded_integer(index, digits)}.png`
  end
end

# after recut and shuffle
def convert_frames(frame_list)
  `mkdir converted_frames`
  digits = frame_length_digit_count(frame_list.flatten)
  effects = [:negate, :oil_paint, :spread]
  frame_counter = 0
  pbar = ProgressBar.create(title: 'Effects', total: frame_list.flatten.count)
  frame_list.each do |cut|
    effects_for_cut = effects.sample(rand(0..4))
    cut.each do |frame|
      frame_img = Magick::Image.read(frame)
      new_frame = frame_img[0]
      effects_for_cut.each do |effect|
        new_frame = new_frame.send(effect)
      end
      new_frame.write("converted_frames/convertedframe#{padded_integer(frame_counter, digits)}.png")
      frame_counter += 1
      pbar.increment
    end
  end
  pbar.finish
end

def frame_length_digit_count(frame_list)
  frame_list.flatten.length.to_s.length
end

def assemble_frames(frame_list, video_name)
  `ffmpeg -framerate 25 -i converted_frames/convertedframe%0#{frame_length_digit_count(frame_list)}d.png -vcodec libx264 -preset slow -crf 22 -threads 0 -r 25 #{video_name}.mkv`
end

def process(video_name, length)
  dump_frames(length.to_i / video_list.length)
  frame_list = shuffle_cuts(recut_frames)
  convert_frames frame_list
  assemble_frames frame_list, video_name
end
