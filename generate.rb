require_relative 'download'
require_relative 'process'

def generate(search_term, length)
  folder_name = friendly_filename(search_term)
  `mkdir #{folder_name}`
  `mkdir #{folder_name}/videos`
  Dir.chdir(folder_name) do
    download_videos (video_download_urls search_term)
    process(search_term, length)
  end
end

if ARGV.count == 2
  generate(ARGV.first, ARGV[1])
else
  puts "Incorrect number of arguments"
end
